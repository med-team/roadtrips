# Uncomment the COMPILER and OPTIONS lines corresponding to
# the platform and compiler you plan to use.

# If compiling with the gcc compiler:
COMPILER = gcc
OPTIONS = -O3

# If compiling with the Sun cc compiler on a Sparc machine.
#COMPILER = cc
#OPTIONS = -fast -xO4

# Generic, should work on work on most machines but may not produce
# the most optimal code
#COMPILER = cc
#OPTIONS = -O


# You should not need to change anything below this line
# Type 'make'.  This will build the program 'ROADTRIPS'.
##################################################################

CC_LIB = -lm

ROADTRIPS:   ROADTRIPS_SOURCE.c cholesky.c pnorms2.c nrutil.c nrutil.h
	$(COMPILER) $(CC_LIB) $(OPTIONS) ROADTRIPS_SOURCE.c -o ROADTRIPS



