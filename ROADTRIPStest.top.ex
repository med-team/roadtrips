Below is a list of the top 20 markers with the smallest p-values using the RM test statistic 

**************************************************

MARKER 678: rs678 has a p-value of 0.0019572
MARKER 228: rs228 has a p-value of 0.00252806
MARKER 375: rs375 has a p-value of 0.00299978
MARKER 505: rs505 has a p-value of 0.00368594
MARKER 635: rs635 has a p-value of 0.00487029
MARKER 57: rs57 has a p-value of 0.00605775
MARKER 669: rs669 has a p-value of 0.00695729
MARKER 272: rs272 has a p-value of 0.00711711
MARKER 612: rs612 has a p-value of 0.00785038
MARKER 343: rs343 has a p-value of 0.00936649
MARKER 958: rs958 has a p-value of 0.0101502
MARKER 252: rs252 has a p-value of 0.0104924
MARKER 712: rs712 has a p-value of 0.0122139
MARKER 867: rs867 has a p-value of 0.013312
MARKER 602: rs602 has a p-value of 0.0134373
MARKER 401: rs401 has a p-value of 0.0140439
MARKER 246: rs246 has a p-value of 0.0147129
MARKER 980: rs980 has a p-value of 0.0154012
MARKER 785: rs785 has a p-value of 0.0155091
MARKER 449: rs449 has a p-value of 0.016115


############################################### 

Below is a list of the top 20 markers with the smallest p-values using the RCHI  test statistic 

**************************************************

MARKER 505: rs505 has a p-value of 0.00101803
MARKER 678: rs678 has a p-value of 0.00227348
MARKER 635: rs635 has a p-value of 0.00437555
MARKER 645: rs645 has a p-value of 0.00484646
MARKER 375: rs375 has a p-value of 0.00484808
MARKER 612: rs612 has a p-value of 0.0057479
MARKER 985: rs985 has a p-value of 0.00582126
MARKER 272: rs272 has a p-value of 0.00707712
MARKER 436: rs436 has a p-value of 0.00763829
MARKER 867: rs867 has a p-value of 0.00882606
MARKER 228: rs228 has a p-value of 0.0106383
MARKER 669: rs669 has a p-value of 0.012155
MARKER 785: rs785 has a p-value of 0.012506
MARKER 343: rs343 has a p-value of 0.0132034
MARKER 57: rs57 has a p-value of 0.0145404
MARKER 641: rs641 has a p-value of 0.0147421
MARKER 458: rs458 has a p-value of 0.0181297
MARKER 246: rs246 has a p-value of 0.0190611
MARKER 858: rs858 has a p-value of 0.0197223
MARKER 741: rs741 has a p-value of 0.0200999


############################################### 

Below is a list of top 20 markers with the smallest p-values using the RW test statistic 

**************************************************

MARKER 252: rs252 has a p-value of 0.000904457
MARKER 224: rs224 has a p-value of 0.00127753
MARKER 900: rs900 has a p-value of 0.00280583
MARKER 962: rs962 has a p-value of 0.00425847
MARKER 528: rs528 has a p-value of 0.00522043
MARKER 612: rs612 has a p-value of 0.00522203
MARKER 708: rs708 has a p-value of 0.00761769
MARKER 380: rs380 has a p-value of 0.00789545
MARKER 155: rs155 has a p-value of 0.00992364
MARKER 996: rs996 has a p-value of 0.0100541
MARKER 869: rs869 has a p-value of 0.0104148
MARKER 757: rs757 has a p-value of 0.0122276
MARKER 842: rs842 has a p-value of 0.0122934
MARKER 564: rs564 has a p-value of 0.0125389
MARKER 179: rs179 has a p-value of 0.0130302
MARKER 821: rs821 has a p-value of 0.0132595
MARKER 275: rs275 has a p-value of 0.0136566
MARKER 381: rs381 has a p-value of 0.013952
MARKER 50: rs50 has a p-value of 0.0153009
MARKER 228: rs228 has a p-value of 0.0162479
